import urllib.request
import json
from GR.settings import MEDIA_ROOT

LOCAL_CAPTCHA_PATH = MEDIA_ROOT +'/captcha.jpg'
ELEME_CAPTCHA_URL = 'https://account.ele.me/restapi/v1/captchas'
ELEME_LOGIN_URL = 'https://account.ele.me/restapi/v1/login'

def get_captcha():
    request = urllib.request.Request(method="POST", url=ELEME_CAPTCHA_URL)
    response = urllib.request.urlopen(request)
    set_cookie = response.getheader('Set-Cookie')
    cookies = set_cookie.split(';')
    login_cookies = ''
    for cookie in cookies:
        if 'eleme__ele_me=' in cookie:
            login_cookies += cookie
            login_cookies += ';'
        elif 'track_id=' in cookie:
            subcookies = cookie.split(',')
            for subcookie in subcookies:
                if 'track_id=' in subcookie:
                    login_cookies += subcookie
                    login_cookies += ';'
    response = json.loads(response.read().decode('utf-8'))
    code = response['code']
    urllib.request.urlretrieve(ELEME_CAPTCHA_URL+'/'+code, LOCAL_CAPTCHA_PATH)
    return login_cookies[:-1]

def web_login(username, pw, captcha, cookie):
    payload = {"username":username,"password":pw,"captcha_code":captcha}
    datas = json.dumps(payload)
    url_values = datas.encode(encoding='utf-8')
    request = urllib.request.Request(method="POST", url=ELEME_LOGIN_URL, data=url_values)
    request.add_header('Cookie', cookie)

    try:
        response = urllib.request.urlopen(request)
    except urllib.error.HTTPError as e:
            error = e.read()
            print(error)
            return {'success': False, 'info': str(error)}

    return {'success': True, 'info': response}
    
