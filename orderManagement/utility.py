'''
Created on Dec 10, 2015

@author: qiwei
'''
import hashlib
from cgi import FieldStorage


def _concat_params(params):
    pairs = []
    for key in sorted(params):
        if key == 'sig':
            continue
        val = params[key]
        if not isinstance(val, FieldStorage):
            pairs.append("{}={}".format(key, val))
    return '&'.join(pairs)


def gen_sig(path_url, params, consumer_secret):
    params = _concat_params(params)
    str = '{}?{}{}'.format(path_url, params, consumer_secret)
    sig = hashlib.new('md5', str).hexdigest().encode('utf-8')
    return sig
